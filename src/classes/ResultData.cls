public with sharing class ResultData {
    public static final String ANSWER_STATUS_SUCCESS           = 'SUCCESS';
    public static final String ANSWER_STATUS_ERROR             = 'ERROR';

    @AuraEnabled public String status = ANSWER_STATUS_ERROR;
    @AuraEnabled public String message = '';
}