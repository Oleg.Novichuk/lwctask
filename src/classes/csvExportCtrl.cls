public with sharing class csvExportCtrl {

    @AuraEnabled
    public static ResultData csvExportToFile(String csvBody) {
        ResultData result = new ResultData();
        ContentVersion file = new ContentVersion(
                title = 'Tax Payments' + Datetime.now().format('yyyy-MM-dd') + '.csv',
                versionData = Blob.valueOf(csvBody),
                pathOnClient = '/Tax Payments.csv'
        );
        try {
            insert file;
            result.status = ResultData.ANSWER_STATUS_SUCCESS;
        } catch (Exception ex) {
            result.status = ResultData.ANSWER_STATUS_ERROR;
            result.message = ex.getMessage();
        }

        return result;
    }
}