public with sharing class TaxOverviewCtrl {

    public final static Integer daysBeforeOverdue = 2;

    public static final Set<String> SOQL_FIELDS = new Set<String>{
        'Id',
        'Income__c',
        'Payment_Date__c',
        'Contact__r.Name',
        'Contact__r.Account.Industry',
        'Contact__r.Account.Tax_Percentage__c',
        'Account__r.Name',
        'Account__r.Industry',
        'Account__r.Tax_Percentage__c'
    };

    @AuraEnabled(Cacheable=true)
    public static List<TaxPaymentWrapper> getTaxPayments(String searchValue, List<String> selectedTypes, Boolean isOverdue) {

        System.debug(selectedTypes);
        String soqlQuery =
            'SELECT '
            + String.join((Iterable<String>) SOQL_FIELDS, ',')
            + ' FROM Tax_Payment__c ';
        String condition = 'WHERE ';

        if (searchValue != '') {
            searchValue = searchValue.trim();
            searchValue = '%' + searchValue + '%';
            condition += '(Contact__r.Name LIKE :searchValue OR Account__r.Name LIKE :searchValue) AND ';
        }
        condition += 'RecordType.Name IN :selectedTypes';


        soqlQuery += condition + ' ORDER BY Payment_Date__c DESC';
        System.debug(soqlQuery);
        return fillWrapper(Database.query(soqlQuery), isOverdue);
    }

    @AuraEnabled
    public static ResultData sendNotification(String idsJSON) {
        System.debug(JSON.deserializeUntyped(idsJSON));

        List<String> ids = (List<String>) JSON.deserialize(idsJSON, List<String>.class);
        Set<Id> recipients = new Set<Id>();
        Set<Id> accounts = new Set<Id>();

        for (Tax_Payment__c taxPayment : [SELECT Id, Account__c, Contact__c, RecordType.Name FROM Tax_Payment__c WHERE Id IN :ids]) {
            if (taxPayment.RecordType.Name == System.Label.Legacy_entity) {
                accounts.add(taxPayment.Account__c);
            } else {
                recipients.add(taxPayment.Contact__c);
            }
        }
        for (Account account : [SELECT Id, (SELECT Main__c, Id FROM Contacts WHERE Main__c = TRUE) FROM Account WHERE Id IN :accounts]) {
            for (Contact contact : account.Contacts) {
                recipients.add(contact.Id);
            }
        }


        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        for (Id id : recipients) {
            System.debug(id);
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

            String subject = 'Notification';
            String body = 'Pay tax';
            email.setTargetObjectId(id);
            email.setSubject(subject);
            email.setHtmlBody(body);
            email.saveAsActivity = false;
            emails.add(email);
        }

        ResultData result = new ResultData();
        List<Messaging.SendEmailResult> emailsResults = Messaging.sendEmail(emails, false);
        for(Messaging.SendEmailResult emailResult : emailsResults) {
            if(!emailResult.isSuccess()) {
                result.status = ResultData.ANSWER_STATUS_ERROR;
                result.message += emailResult.getErrors();
            }
        }
        if (result.message == '') {
            result.status = ResultData.ANSWER_STATUS_SUCCESS;
        }
        return result;
    }

    public static List<TaxPaymentWrapper> fillWrapper(List<Tax_Payment__c> taxPayments, Boolean isOverdue){
        Map<String, TaxPaymentWrapper> taxPaymentWrappersByName = new Map<String, TaxPaymentWrapper>();
        for (Tax_Payment__c taxPayment : taxPayments) {
            TaxPaymentWrapper paymentWrapper = new TaxPaymentWrapper();
            paymentWrapper.id = taxPayment.Id;

            if (taxPayment.Account__r.Name == null) {
                paymentWrapper.name = taxPayment.Contact__r.Name;
                paymentWrapper.industry = taxPayment.Contact__r.Account.Industry;
                paymentWrapper.percentage = taxPayment.Contact__r.Account.Tax_Percentage__c / 100;
            } else {
                paymentWrapper.name = taxPayment.Account__r.Name;
                paymentWrapper.industry = taxPayment.Account__r.Industry;
                paymentWrapper.percentage = taxPayment.Account__r.Tax_Percentage__c / 100;
            }
            paymentWrapper.income = taxPayment.Income__c;
            paymentWrapper.paymentDate = taxPayment.Payment_Date__c;

            if (!taxPaymentWrappersByName.keySet().contains(paymentWrapper.name)) {
                taxPaymentWrappersByName.put(paymentWrapper.name, paymentWrapper);
            }
        }

        if (isOverdue) {
            for (String name : taxPaymentWrappersByName.keySet()) {
                if (taxPaymentWrappersByName.get(name).paymentDate.addMonths(1) > Date.today() + daysBeforeOverdue) {
                    taxPaymentWrappersByName.remove(name);
                }
            }
        }
        return taxPaymentWrappersByName.values();
    }

    public class TaxPaymentWrapper{
        @AuraEnabled public String  id;
        @AuraEnabled public String  name;
        @AuraEnabled public String  industry;
        @AuraEnabled public Decimal income;
        @AuraEnabled public Decimal percentage;
        @AuraEnabled public Date    paymentDate;
        @AuraEnabled public String  notify;

        public TaxPaymentWrapper() {
            notify = System.Label.Notify;
        }
    }

}