import LightningDatatable from 'lightning/datatable';
import customButtonTemplate from './customButtonTemplate.html';

export default class CustomDatatable extends LightningDatatable {
    static customTypes = {
        customButton: {
            template: customButtonTemplate,
            standardCellLayout: true,
            typeAttributes: ['rowId', 'buttonLabel']
        }
    };
}