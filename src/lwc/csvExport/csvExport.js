import { LightningElement, api, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import exportLabel from '@salesforce/label/c.Export';
import exportSettings from '@salesforce/label/c.Export_settings';
import done from '@salesforce/label/c.Done';
import cancel from '@salesforce/label/c.Cancel';
import saveLocally from '@salesforce/label/c.Save_locally';
import saveInDocumentsFolder from '@salesforce/label/c.Save_in_Documents_folder';
import success from '@salesforce/label/c.Success';
import error from '@salesforce/label/c.Error';

import csvExportToFile from '@salesforce/apex/csvExportCtrl.csvExportToFile';

const LABELS = {
    exportLabel : exportLabel,
    exportSettings: exportSettings,
    done: done,
    cancel: cancel,
    saveLocally: saveLocally,
    saveInDocumentsFolder: saveInDocumentsFolder,
    success: success,
    error: error
};

export default class CsvExport extends LightningElement {
    labels = LABELS;

    @api csvColumns;
    @api csvData;

    @track showModal = false;

    exportValue = [];

    @track disableDoneButton = true;

    get exportOptions() {
        return [
            { label: this.labels.saveLocally,           value: this.labels.saveLocally },
            { label: this.labels.saveInDocumentsFolder, value: this.labels.saveInDocumentsFolder },
        ];
    }

    handleChangeExportValue(event) {
        this.exportValue = event.detail.value;

        if (this.exportValue.length > 0) {
            this.disableDoneButton = false;
        } else {
            this.disableDoneButton = true;
        }
    }

    openModal() {
        this.showModal = true;
    }

    closeModal() {
        this.showModal = false;
    }

    handleExport(){
        let rowEnd = '\n';
        let csvString = '';
        let rowMap = new Map();

        this.csvColumns.forEach(function (record) {
            rowMap.set(record.fieldName, record.label)
        });

        let rowData = Array.from(rowMap.values());

        csvString += rowData.join(',');
        csvString += rowEnd;

        rowData = Array.from(rowMap.keys());

        for(let i=0; i < this.csvData.length; i++){
            let colValue = 0;

            for(let key in rowData) {
                if(rowData.hasOwnProperty(key)) {
                    let rowKey = rowData[key];
                    if(colValue > 0){
                        csvString += ',';
                    }
                    let value = this.csvData[i][rowKey] === undefined ? '' : this.csvData[i][rowKey];
                    csvString += '"'+ value +'"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }
        var today = new Date(),
            date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(),
            context = this;
        this.exportValue.forEach(function (option) {
            if (option === context.labels.saveLocally) {
                let downloadElement = document.createElement('a');
                downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
                downloadElement.target = '_self';
                downloadElement.download = 'Tax Payments' + date + '.csv';
                document.body.appendChild(downloadElement);
                downloadElement.click();
            }
            if (option === context.labels.saveInDocumentsFolder) {
                csvExportToFile({csvBody: csvString})
                .then(result => {
                    if (result.status === 'SUCCESS') {
                        context.showToast(context.labels.success, 'Export completed', 'success');
                    } else {
                        context.showToast(context.labels.error, result.message, 'error');
                    }
                })
                .catch(error => {
                    context.showToast(context.labels.error, 'Export error', 'error');
                })
            }
        });
        this.showModal = false;
    }

    showToast(title, message, variant) {
        const toast = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(toast);
    }
}