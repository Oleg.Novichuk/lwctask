import { LightningElement, api } from "lwc";

export default class ButtonMenuOnselect extends LightningElement {
    @api rowId;
    @api buttonLabel;

    handleClick() {
        const event = CustomEvent('customclick', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                rowId: this.rowId
            },
        });
        this.dispatchEvent(event);
    }
}