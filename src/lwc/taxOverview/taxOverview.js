import { LightningElement, wire, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

/*=========================================== CUSTOM APEX ===========================================================*/
import searchTaxPayments from '@salesforce/apex/TaxOverviewCtrl.getTaxPayments';
import sendNotification from '@salesforce/apex/TaxOverviewCtrl.sendNotification';

/*========================================== CUSTOM LABELS ==========================================================*/
import recentPayments from '@salesforce/label/c.Recent_Payments';
import overduePayments from '@salesforce/label/c.Overdue_Payments';
import searchByName from '@salesforce/label/c.Search_by_Name';
import legacyEntity from '@salesforce/label/c.Legacy_entity';
import individuals from '@salesforce/label/c.Individuals';
import taxpayerName from '@salesforce/label/c.Taxpayer_Name';
import industry from '@salesforce/label/c.Industry';
import income from '@salesforce/label/c.Income';
import taxPercentage from '@salesforce/label/c.Tax_Percentage';
import date from '@salesforce/label/c.Date';
import notifyAll from '@salesforce/label/c.Notify_All';
import success from '@salesforce/label/c.Success';
import error from '@salesforce/label/c.Error';

const LABELS = {
    recentPayments : recentPayments,
    overduePayments : overduePayments,
    searchByName : searchByName,
    legacyEntity : legacyEntity,
    individuals : individuals,
    taxpayerName : taxpayerName,
    industry : industry,
    income : income,
    taxPercentage : taxPercentage,
    date : date,
    notifyAll : notifyAll,
    success : success,
    error : error,
};

const PAGE_SIZE = 2;

export default class TaxOverview extends LightningElement {
    labels = LABELS;
    pageSize = PAGE_SIZE;

    @track columns;

    searchValue = '';
    filterValue = [this.labels.legacyEntity, this.labels.individuals];
    isOverdue = null;

    @track allTaxPayments;
    @track afterPaginationTaxPayments;
    @track showTable = false;

    fillPaginationTaxPayments(event) {
        console.log(JSON.stringify(event.detail));
        this.afterPaginationTaxPayments = event.detail;
    }

    @wire(searchTaxPayments, {searchValue: '$searchValue', selectedTypes: '$filterValue', isOverdue: '$isOverdue'})
    taxPayments({ error, data }) {
        if (data) {
            this.allTaxPayments = data;
            this.showTable = true;
        } else if (error) {
            this.allTaxPayments = undefined;
        }
    }

    get filterOptions() {
        return [
            { label: this.labels.legacyEntity, value: this.labels.legacyEntity },
            { label: this.labels.individuals, value: this.labels.individuals },
        ];
    }

    handleChangeFilterValue(event) {
        this.showTable = false;

        this.filterValue = event.detail.value;
    }

    handleActiveTab(event) {
        this.showTable = false;

        var columns = [
            {label: LABELS.taxpayerName, fieldName: 'name', hideDefaultActions: "true"},
            {label: LABELS.industry, fieldName: 'industry', hideDefaultActions: "true"},
            {label: LABELS.income, fieldName: 'income', type: 'currency', hideDefaultActions: "true"},
            {label: LABELS.taxPercentage, fieldName: 'percentage', type: 'percent', hideDefaultActions: "true"}
        ];

        if (event.target.value == this.labels.overduePayments) {
            this.isOverdue = true;
            columns.push({
                label: '', fieldName: 'button', type: 'customButton',
                typeAttributes: {
                    rowId: {fieldName: 'id'},
                    buttonLabel: {fieldName: 'notify'}
                },
                hideDefaultActions: "true"
            });
        } else {
            this.isOverdue = false;
            columns.push({
                label: LABELS.date,
                fieldName: 'paymentDate',
                type: 'date-local',
                hideDefaultActions: "true"
            });
        }
        this.columns = columns;
    }

    handleSearchEvent(event) {
        this.showTable = false;

        this.searchValue = event.detail;
    }

    handleNotify(event) {
        this.sendMessage(event.detail.rowId);
    }

    handleNotifyAll() {
        this.sendMessage(this.afterPaginationTaxPayments);
    }

    sendMessage(data) {
        var recordIds = [];

        if (Array.isArray(data)) {
            data.forEach(function (record) {
                recordIds.push(record.id);
            });
        } else {
            recordIds.push(data);
        }

        sendNotification({idsJSON: JSON.stringify(recordIds)})
        .then(result => {
            if (result.status === 'SUCCESS') {
                this.showToast(this.labels.success, 'All notification sent', 'success');
            } else {
                this.showToast(this.labels.error, result.message, 'error');
            }
        })
        .catch(error => {
            this.showToast(this.labels.error, 'Notifications error', 'error');
        })
    }

    showToast(title, message, variant) {
        const toast = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(toast);
    }
}