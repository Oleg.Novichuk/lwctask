import { LightningElement, api } from 'lwc';

export default class SearchString extends LightningElement {
    @api label;

    handleSearchValue(event) {
        const searchValue = event.target.value;
        window.clearTimeout(this.delayTimeout);
        this.delayTimeout = setTimeout(() => {
            this.dispatchEventToParrent(searchValue);
        }, 300);
    }

    dispatchEventToParrent(searchValue) {
        const selectedEvent = new CustomEvent("searchevent", {
            detail: searchValue
        });
        this.dispatchEvent(selectedEvent);
    }
}