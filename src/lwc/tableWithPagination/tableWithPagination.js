import { LightningElement, api, track } from 'lwc';

export default class TableWithPagination extends LightningElement {
    page = 1;

    @api columns;
    @api pageSize;
    @api tableData;

    @track afterPaginationData;
    @track totalRecords = 0;

    connectedCallback() {
        this.fillData();
        this.totalRecords = this.tableData.length;
    }

    fillData(){
        this.afterPaginationData = [];
        for (var i = this.pageSize * (this.page -1); i < this.page * this.pageSize; i++) {
            this.afterPaginationData.push(this.tableData[i]);
        }
        const selectedEvent = new CustomEvent("changepaginationdata", {
            detail: this.afterPaginationData
        });
        this.dispatchEvent(selectedEvent);
    }

    handlePrevious() {
        if (this.page > 1) {
            this.page = this.page - 1;
        }
        this.fillData();
    }

    handleNext() {
        if (this.page < this.tableData.length) {
            this.page = this.page + 1;
        }
        this.fillData();
    }

    handleFirst() {
        this.page = 1;
        this.fillData();
    }

    handleLast() {
        this.page = Math.ceil(this.tableData.length / this.pageSize);
        this.fillData();
    }
}